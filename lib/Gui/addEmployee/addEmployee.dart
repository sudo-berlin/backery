import 'package:bakery_employee_controller/classes/employee.dart';
import 'package:flutter/material.dart';
import 'package:bakery_employee_controller/Gui/widgets.dart';

class AddEmployee extends StatefulWidget {
  @override
  _AddEmployeeState createState() => _AddEmployeeState();
}

class _AddEmployeeState extends State<AddEmployee> {
  TextEditingController name = TextEditingController(),
      password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.blue[700],
        title: new Text(
          "Add Employee",
          style: new TextStyle(color: Colors.white),
          textScaleFactor: 1,
        ),
        centerTitle: true,
        toolbarHeight: 80,
      ),
      body: Container(
          alignment: Alignment.center,
          width: width,
          height: height,
          color: Colors.white,
          padding: EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset('employee.jpg'),
                new Text("\n"),
                new TextField(
                  controller: name,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      hintText: 'Employee Name',
                      icon: Icon(
                        Icons.person,
                        color: Colors.blue[700],
                      )),
                ),
                new TextField(
                  controller: password,
                  decoration: InputDecoration(
                      hintText: 'Password',
                      icon: Icon(
                        Icons.vpn_key,
                        color: Colors.blue[700],
                      )),
                ),
                new Text(
                  '\n',
                  textScaleFactor: 1,
                ),
                new Text(
                  '\n',
                  textScaleFactor: 1,
                ),
                new SizedBox(
                  width: width,
                  height: 50,
                  child: new ElevatedButton(
                      onPressed: () async {
                        if (checker(name.text) && checker(password.text)) {
                          loading(context, width);
                          if (await Employee(
                                      userName: name.text,
                                      password: password.text)
                                  .register() ==
                              true) {
                            name.clear();
                            password.clear();
                            Navigator.pop(context);
                            Navigator.pop(context);
                          } else {
                            Navigator.pop(context);
                            error1(context, width, "No Network Connection");
                          }
                        } else {
                          error1(context, width, "Invalid Info");
                        }
                      },
                      style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all(Colors.blue[700]),
                        backgroundColor:
                            MaterialStateProperty.all(Colors.blue[700]),
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12))),
                      ),
                      child: new Text(
                        "Add Employee",
                        textScaleFactor: 1,
                        style: new TextStyle(color: Colors.white),
                      )),
                )
              ],
            ),
          )),
    );
  }
}
