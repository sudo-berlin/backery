import 'dart:async';

import 'package:bakery_employee_controller/Gui/home/home.dart';
import 'package:bakery_employee_controller/classes/employee.dart';
import 'package:flutter/material.dart';

void loading(var context, var width) {
  showModalBottomSheet(
    context: context,
    builder: (context) {
      Timer(Duration(seconds: 5), () {
        Navigator.pop(context);
        error1(context, width, "No Network !");
      });
      return new Container(
        width: width,
        height: 80,
        alignment: Alignment.center,
        child: new CircularProgressIndicator(),
      );
    },
    isDismissible: false,
    isScrollControlled: false,
  );
}

bool checker(String s) => (s.indexOf(" ") == -1 && s.isNotEmpty ? true : false);

void viewer(var context, Employee employee) {
  TextEditingController name = TextEditingController(text: employee.userName),
      password = TextEditingController(text: employee.password);

  showDialog(
      context: context,
      builder: (context) {
        var height = MediaQuery.of(context).size.height;
        return AlertDialog(
          title: new Text(
            "Employee info",
            textScaleFactor: 1,
            style: new TextStyle(color: Colors.blue[700]),
          ),
          content: new Container(
            alignment: Alignment.center,
            width: 2000,
            height: 400,
            child: new ListView(
              children: [
                new Image.asset(
                  "person.jpg",
                  height: height * 0.3,
                ),
                new TextField(
                  controller: name,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    hintText: 'Employee Name',
                    labelText: "Employee Name",
                    disabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.blue[700])),
                    labelStyle: new TextStyle(color: Colors.blue[700]),
                    icon: Icon(
                      Icons.person,
                      color: Colors.blue[700],
                    ),
                  ),
                  enabled: false,
                ),
                new TextField(
                  controller: password,
                  decoration: InputDecoration(
                      hintText: 'Password',
                      labelText: "Employee Password",
                      disabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue[700])),
                      labelStyle: new TextStyle(color: Colors.blue[700]),
                      icon: Icon(
                        Icons.vpn_key,
                        color: Colors.blue[700],
                      )),
                  enabled: false,
                ),
              ],
            ),
          ),
          actions: [
            new TextButton(
                onPressed: () async {
                  loading(context, 2000.0);
                  if (await employee.delete()) {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (builder) => HomePage()),
                        (route) => false);
                  } else {
                    Navigator.pop(context);
                    error1(context, 2000.0, "No Network !");
                  }
                },
                child: new Text(
                  "Delete",
                  style: new TextStyle(color: Colors.red),
                )),
            new TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: new Text("OK")),
          ],
        );
      });
}

void error1(var context, var width, String error) {
  showModalBottomSheet(
    context: context,
    builder: (context) {
      Timer(Duration(seconds: 2), () {
        Navigator.pop(context);
      });
      return new Container(
        width: width,
        height: 80,
        color: Colors.red,
        alignment: Alignment.center,
        child: new Text(
          error,
          style: new TextStyle(color: Colors.white, fontSize: 15),
          textScaleFactor: 1,
        ),
      );
    },
    isDismissible: true,
    isScrollControlled: true,
  );
}
