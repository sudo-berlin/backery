import 'package:bakery_employee_controller/Gui/widgets.dart';
import 'package:bakery_employee_controller/classes/employee.dart';
import 'package:flutter/material.dart';

Widget noData(var w, var h) {
  return new Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Image.asset(
        "no-data.jpg",
        width: w * 0.6,
        height: h * 0.3,
      ),
      new Text("No Employee Added Yet !",
          style: new TextStyle(fontSize: 15, fontWeight: FontWeight.w300),
          textScaleFactor: 1,
          textAlign: TextAlign.center)
    ],
  );
}

Widget holder(var width, var height, var context, Employee employee) {
  return Container(
    width: width,
    height: 150,
    alignment: Alignment.center,
    child: new Container(
      width: width,
      height: 120,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(
              width: 5, color: Colors.blue[700], style: BorderStyle.solid),
          boxShadow: [
            BoxShadow(blurRadius: 5, spreadRadius: 5, color: Colors.blue[700])
          ]),
      child: ElevatedButton(
        child: new Text(
          employee.userName,
          style: new TextStyle(
              fontSize: 20, color: Colors.black, fontWeight: FontWeight.w400),
          textScaleFactor: 1,
        ),
        onPressed: () {
          viewer(context, employee);
        },
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.white),
            foregroundColor: MaterialStateProperty.all(Colors.grey),
            overlayColor: MaterialStateProperty.all(Colors.grey[300]),
            shape: MaterialStateProperty.all(RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15)))),
      ),
    ),
  );
}
