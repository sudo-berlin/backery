import 'package:bakery_employee_controller/Gui/addEmployee/addEmployee.dart';
import 'package:bakery_employee_controller/Gui/home/custom.dart';
import 'package:bakery_employee_controller/classes/employee.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.blue[700],
        title: new Text(
          "Your Employee",
          style: new TextStyle(color: Colors.white),
          textScaleFactor: 1,
        ),
        centerTitle: true,
        toolbarHeight: 80,
        actions: [
          IconButton(
              icon: Icon(
                Icons.add,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddEmployee()));
              })
        ],
      ),
      body: new Container(
          width: width,
          height: height,
          padding: EdgeInsets.all(10),
          color: Colors.white,
          alignment: Alignment.center,
          child: FutureBuilder(
            future: Employee.getEmployee(),
            builder: (context, snapshot) {
              print(snapshot.data);
              if (snapshot.connectionState == ConnectionState.done &&
                  snapshot.data == false) {
                return new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Icon(
                      Icons.wifi_off,
                      color: Colors.grey,
                      size: 50,
                    ),
                    new Text(
                      "No Network !",
                      textScaleFactor: 1,
                    )
                  ],
                );
              } else if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicator();
              } else if (snapshot.hasData) {
                return ListView.builder(
                  physics: AlwaysScrollableScrollPhysics(),
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    int id = (snapshot.data[index]["id"] is int
                        ? snapshot.data[index]["id"]
                        : int.parse(snapshot.data[index]["id"]));

                    return holder(
                        width,
                        height,
                        context,
                        Employee(
                            id: id,
                            userName: snapshot.data[index]["user"],
                            password: snapshot.data[index]["pass"]));
                  },
                );
              } else {
                return noData(width, height);
              }
            },
          )),
    );
  }
}
