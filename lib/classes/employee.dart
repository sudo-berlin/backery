import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class Employee {
  var userName;
  var password;
  var id = -1;

  Employee({@required this.id, this.userName, this.password});

  dynamic getMap() {
    var data = {};

    data.putIfAbsent('user', () => userName);
    data.putIfAbsent('pass', () => password);
    data.putIfAbsent('id', () => id);
    return data;
  }

  Future<dynamic> register() async {
    try {
      var rep = await http.post(Uri.parse('https://www.google.com/'));
    } catch (e) {
      return false;
    }
    var rep = await http.post(
        Uri.parse(
            'https://vaccinial-intercom.000webhostapp.com/Salem_Bakery/employee.php'),
        body: {'op': 'get'});
    if (jsonDecode(rep.body)["Employee"] is Map) {
      this.id = 2;
      var rep = await http.post(
          Uri.parse(
              'https://vaccinial-intercom.000webhostapp.com/Salem_Bakery/employee.php'),
          body: {
            'op': 'ADD',
            'user': userName,
            'password': password,
            "id": this.id.toString()
          });

      return true;
    } else if (jsonDecode(rep.body)["Employee"] is List) {
      int idd = jsonDecode(rep.body)["Employee"].length;
      this.id = int.parse(jsonDecode(rep.body)["Employee"][idd - 1]["id"]) + 1;
      var rep1 = await http.post(
          Uri.parse(
              'https://vaccinial-intercom.000webhostapp.com/Salem_Bakery/employee.php'),
          body: {
            'op': 'ADD',
            'user': userName,
            'password': password,
            "id": this.id.toString()
          });
      return true;
    } else {
      this.id = 1;
      var rep1 = await http.post(
          Uri.parse(
              'https://vaccinial-intercom.000webhostapp.com/Salem_Bakery/employee.php'),
          body: {
            'op': 'ADD',
            'user': userName,
            'password': password,
            "id": this.id.toString()
          });
      return true;
    }
  }

  static Future<dynamic> getEmployee() async {
    try {
      var rep1 = await http.get(Uri.parse('https://www.google.com/'));
    } catch (e) {
      return false;
    }
    var rep = await http.post(
        Uri.parse(
            'https://vaccinial-intercom.000webhostapp.com/Salem_Bakery/employee.php'),
        body: {'op': 'get'});

    print(jsonDecode(rep.body)["Employee"]);
    if (jsonDecode(rep.body)["Employee"] is List) {
      return jsonDecode(rep.body)["Employee"];
    } else if (jsonDecode(rep.body)["Employee"] is Map) {
      var list = [];
      Employee x = new Employee(
          id: int.parse(jsonDecode(rep.body)["Employee"]["id"]),
          userName: jsonDecode(rep.body)["Employee"]["user"],
          password: jsonDecode(rep.body)["Employee"]["pass"]);
      list.add(x.getMap());
      return list;
    }
  }

  Future<bool> delete() async {
    try {
      var rep = await http.post(Uri.parse('https://www.google.com/'));
    } catch (e) {
      return false;
    }
    var rep = await http.post(
        Uri.parse(
            'https://vaccinial-intercom.000webhostapp.com/Salem_Bakery/employee.php'),
        body: {'op': 'del', 'id': id.toString()});
    return true;
  }
}
